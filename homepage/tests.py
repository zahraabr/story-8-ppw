from django.test import TestCase, LiveServerTestCase
from django.test.client import Client
from django.urls import resolve
from .views import *
from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
import time


# Create your tests here.
class Story8UnitTest (TestCase):
    def test_homepage_url_template_and_function(self):
        response = Client().get('')
        found = resolve('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')
        self.assertEqual(found.func, index)

class Story8FunctionalTest(LiveServerTestCase):
    def setUp(self) :
        # super().setUp()
        # chrome_options = webdriver.ChromeOptions()
        # self.driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver')

        # chrome_options = Options()
        # chrome_options.add_argument('--dns-prefetch-disable')
        # chrome_options.add_argument('--no-sandbox')        
        # chrome_options.add_argument('--headless')
        # chrome_options.add_argument('disable-gpu')
        # self.driver = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        # super(Story8FunctionalTest,self).setUp()

        # chrome_options = Options()
        # chrome_options.add_argument('--headless')
        # chrome_options.add_argument('--no-sandbox')
        # chrome_options.add_argument('--disable-dev-shm-usage')
        # self.selenium = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
        # super(Story8FunctionalTest, self).setUp()

        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument('--disable-dev-shm-usage')
        self.driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver')

    
    def tearDown(self) :
        self.driver.quit()
        super().tearDown()

    def test_accordion(self):
        self.driver.get(self.live_server_url)
        response_page = self.driver.page_source

        self.assertIn("about me", response_page)
        self.assertIn("i'm currently . . .", response_page)
        self.assertIn("experiences", response_page)
        self.assertIn("skills", response_page)

    def test_accordion_content(self):
        self.driver.get(self.live_server_url)
        response_content = self.driver.page_source

        accordion_bar = self.driver.find_element_by_class_name('col-sm-10')
        self.assertFalse(accordion_bar.get_attribute('aria-expanded'))

        accordion_bar.click()
        time.sleep(3)
        self.assertTrue(accordion_bar.get_attribute('aria-expanded'))
    
    def test_up_down_arrows(self) :
        self.driver.get(self.live_server_url)
        response_page = self.driver.page_source

        up = self.driver.find_element_by_id('oneUp')
        down = self.driver.find_element_by_id('oneDown')

        know = response_page.find('about me')
        activity = response_page.find("i'm currently . . .")
        self.assertTrue(know < activity)

        time.sleep(2)
        down.click()
        response_page = self.driver.page_source

        time.sleep(2)
        getToKnowMe = response_page.find('about me')
        activity = response_page.find("i'm currently . . .")
        self.assertTrue(getToKnowMe > activity)

        time.sleep(2)
        up.click()

        time.sleep(2)
        response_page = self.driver.page_source
        getToKnowMe = response_page.find('about me')
        activity = response_page.find("i'm currently . . .")
        self.assertTrue(getToKnowMe < activity)

    def test_change_theme(self) :
        self.driver.get(self.live_server_url)
        response_page = self.driver.page_source

        light_bg_color = self.driver.find_element_by_class_name('light').value_of_css_property('background-color')
        self.assertEqual(light_bg_color, 'rgba(247, 236, 223, 1)')

        light_title_text_color = self.driver.find_element_by_class_name('light').value_of_css_property('color')
        self.assertEqual(light_title_text_color, 'rgba(239, 151, 46, 1)')

        light_box_color = self.driver.find_element_by_class_name('row').value_of_css_property('background-color')
        self.assertEqual(light_box_color, 'rgba(250, 176, 131, 1)')

        light_up_color = self.driver.find_element_by_class_name('move-up').value_of_css_property('background-color')
        self.assertEqual(light_up_color, 'rgba(255, 211, 182, 1)')

        light_down_color = self.driver.find_element_by_class_name('move-down').value_of_css_property('background-color')
        self.assertEqual(light_down_color, 'rgba(255, 211, 182, 1)')

        light_desc_color = self.driver.find_element_by_tag_name('p').value_of_css_property('color')
        self.assertEqual(light_desc_color, 'rgba(68, 68, 68, 1)')

        light_desc_color = self.driver.find_element_by_tag_name('li').value_of_css_property('color')
        self.assertEqual(light_desc_color, 'rgba(68, 68, 68, 1)')

        time.sleep(2)
        self.driver.find_element_by_tag_name('label').click()
        time.sleep(2)

        night_bg_color = self.driver.find_element_by_class_name('night').value_of_css_property('background-color')
        self.assertEqual(night_bg_color, 'rgba(0, 0, 0, 1)')

        night_title_text_color = self.driver.find_element_by_class_name('night').value_of_css_property('color')
        self.assertEqual(night_title_text_color, 'rgba(255, 255, 255, 1)')

        night_box_color = self.driver.find_element_by_class_name('row').value_of_css_property('background-color')
        self.assertEqual(night_box_color, 'rgba(153, 153, 153, 1)')

        night_up_color = self.driver.find_element_by_class_name('move-up').value_of_css_property('background-color')
        self.assertEqual(night_up_color, 'rgba(0, 0, 0, 1)')

        night_down_color = self.driver.find_element_by_class_name('move-down').value_of_css_property('background-color')
        self.assertEqual(night_down_color, 'rgba(0, 0, 0, 1)')

        night_desc_color = self.driver.find_element_by_class_name('desc').value_of_css_property('color')
        self.assertEqual(night_desc_color, 'rgba(128, 128, 128, 1)')

        night_desc_color = self.driver.find_element_by_tag_name('p').value_of_css_property('color')
        self.assertEqual(night_desc_color, 'rgba(255, 255, 255, 1)')

        night_desc_color = self.driver.find_element_by_tag_name('li').value_of_css_property('color')
        self.assertEqual(night_desc_color, 'rgba(255, 255, 255, 1)')